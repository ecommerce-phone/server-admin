<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'title',
        'slug',
        'content',
        'content_type',
        'thumbnails',
        'source',
        'start_date',
        'end_date'
    ];

    protected $filter = [
        'id',
        'title',
        'slug',
        'content',
        'content_type',
        'thumbnails',
        'source',
        'start_date',
        'end_date'
    ];
}
