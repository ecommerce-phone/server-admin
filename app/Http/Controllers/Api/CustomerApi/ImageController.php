<?php

namespace App\Http\Controllers;

use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImageController extends AbstractApiController
{
    /**
     * Upload hình vào bucket tạm và tạo các hình thu nhỏ với kích thức khác nhau
     *
     * @param  Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        if (! $request->hasFile('file')) {
            $this->setStatusCode(400);
            return $this->respond();
        }

        $bucket_prefix = config('filesystems.bucket_prefix', '');

        $slugify          = new Slugify();
        $bucket_path      = 'images/post/'.$bucket_prefix.now()->toDateString();
        $file             = $request->file('file');
        $fileOriginalName = $file->getClientOriginalName();
        $fileExtension    = $file->getClientOriginalExtension();
        $fileName         = str_replace('.'.$fileExtension, '', $fileOriginalName);
        $fileName         = $slugify->slugify($fileName);

        // Bug fix request timeout
        set_time_limit(90);
        ini_set('memory_limit', '256M');

        $_t    = substr($fileOriginalName, 0, 4);
        $is360 = $_t === '360_' || $_t === 'PANO';

        if (! $is360) {
            $expect_size = [
                'o'  => 1080,
                'xs' => 360,
            ];
        } else {
            $expect_size = [
                'o'  => 5000,
                'xs' => 360,
            ];
        }

        $result = [
            'filepath' => $bucket_path,
            'type'     => ! $is360 ? 'image' : 'image_360',
            'files'    => [],
        ];

        foreach ($expect_size as $ranting => $size) {
            $img = Image::make($file);
            $img->resize($size, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $newFilename = $fileName.'_'.time().'_'.$ranting.'.jpg';

            $img->save('images/product/'.$newFilename, 80, 'jpg');

//            $file->move(public_path('images/post'), $newFilename);

//            Storage::disk('local_public')->put($bucket_path.'/'.$newFilename, public_path());

//            unlink('images/post/'.$newFilename);

            array_push($result['files'], [
                'filename' => $newFilename,
                'ranting'  => $ranting,
                'width'    => $img->getWidth(),
                'height'   => $img->getHeight(),
                // 'driver'   => $img->getDriver()->getDriverName(),
            ]);

            $img->destroy();
        }


        return $this->item($result);
    }
}