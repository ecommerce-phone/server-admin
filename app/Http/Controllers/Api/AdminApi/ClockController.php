<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\ClockImage;
use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\ClockCreateRequest;
use App\Clock;
use App\ProductImage;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClockController extends AbstractApiController
{
    public function index(Request $request)
    {
        $clock = Clock::query()
            ->select([
                'id',
                'name',
                'slug',
                'price',
                'description',
                'thumbnails',
                'face_diameter',
                'type_machine',
                'wire_material',
                'frame_material',
                'face_thickness',
                'material_glass',
                'utilities',
                'waterproof',
                'wire_width',
                'replace_cord',
                'energy_sources',
                'battery_life',
                'subjects_used',
                'trademark',
                'manufacture',
                'start_date',
                'end_date'
            ])
            ->DataTablePaginate($request);

        return $this->item($clock);
    }

    public function create(ClockCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['name']                                    = $validatedData['name'];
        $payload['slug']                                    = $slugify->slugify($validatedData['name']);

        $payload['price']                                   = $validatedData['price'];
        $payload['description']                             = $validatedData['description'];

        $payload['face_diameter']                           = $validatedData['face_diameter'];
        $payload['type_machine']                            = $validatedData['type_machine'];
        $payload['wire_material']                           = $validatedData['wire_material'];
        $payload['frame_material']                          = $validatedData['frame_material'];
        $payload['face_thickness']                          = $validatedData['face_thickness'];
        $payload['material_glass']                          = $validatedData['material_glass'];
        $payload['utilities']                               = $validatedData['utilities'];
        $payload['waterproof']                              = $validatedData['waterproof'];
        $payload['wire_width']                              = $validatedData['wire_width'];

        // Hình ảnh
        $payload['images']                                  = ! empty($validatedData['images']) ? $validatedData['images'] : [];

        $payload['replace_cord']                            = $validatedData['replace_cord'];
        $payload['energy_sources']                          = $validatedData['energy_sources'];
        $payload['battery_life']                            = $validatedData['battery_life'];
        $payload['subjects_used']                           = $validatedData['subjects_used'];

        $payload['thumbnails']                              = $validatedData['thumbnails'];
        $payload['trademark']                               = $validatedData['trademark'];
        $payload['manufacture']                             = $validatedData['manufacture'];

        $payload['start_date']                              = $validatedData['start_date'];
        $payload['end_date']                                = $validatedData['end_date'];

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên sản phẩm');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $clock = Clock::create($payload);
        DB::beginTransaction();

        if (! empty($payload['images'])) {

//                foreach ($payload['images'] as $image_idx => $image) {
//                    $currentBucketPath = $image['filepath'];
//
//                    foreach ($image['uploaded_data'] as $thumb_index => $targetFile) {
//                        Storage::disk('minio')->copy(
//                            $currentBucketPath.'/'.$targetFile['filename'],
//                            $newBucketPath.'/'.$targetFile['filename']
//                        );
//
//                        $payload['images'][$image_idx]['filepath'] = $newBucketPath;
//                    }
//                }

            // Cập nhật bucket path
            $clock->addImages($payload['images']);
        }

        try {
            $clock->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm sản phẩm thành công!');
            $this->setStatusCode(200);
            $this->setData($clock);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
//        return Post::query()->findOrFail($id);


//        $images = ProductImage::query()->where('post_id', '=', $id)->get();
//        $product = Post::query()->findOrFail($id);
//
//        $result = [
//            'images' => $images,
//            'post'     => $product,
//        ];
//
//        return $this->item($result);
        $query = Clock::query();
        $query->where('id', '=', $id);
        $product = $query->firstOrFail();
        $product->load('images');

        return $this->item($product);
    }

    public function RemoveImagesOld($id)
    {
        return ClockImage::query()->where('id', '=', $id)->delete();
    }

    public function update(ClockCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $clock = Clock::query()->findOrFail($id);
        if (!$clock) {
            $this->setMessage('Không có sản phẩm này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $clock->name                                    = $validatedData['name'];
                $clock->slug                                    = $slugify->slugify($validatedData['name']);
                $clock->price                                   = $validatedData['price'];
                $clock->description                             = $validatedData['description'];

                $clock->face_diameter                           = $validatedData['face_diameter'];
                $clock->type_machine                            = $validatedData['type_machine'];
                $clock->wire_material                           = $validatedData['wire_material'];
                $clock->frame_material                          = $validatedData['frame_material'];
                $clock->face_thickness                          = $validatedData['face_thickness'];
                $clock->material_glass                          = $validatedData['material_glass'];
                $clock->utilities                               = $validatedData['utilities'];
                $clock->waterproof                              = $validatedData['waterproof'];
                $clock->wire_width                              = $validatedData['wire_width'];
                $clock->replace_cord                            = $validatedData['replace_cord'];
                $clock->energy_sources                          = $validatedData['energy_sources'];
                $clock->battery_life                            = $validatedData['battery_life'];
                $clock->subjects_used                           = $validatedData['subjects_used'];
                $clock->trademark                               = $validatedData['trademark'];
                $clock->manufacture                             = $validatedData['manufacture'];

                $payload['images']                              = ! empty($validatedData['images']) ? $validatedData['images'] : [];
                $payload['imagesOld']                           = ! empty($validatedData['imagesOld']) ? $validatedData['imagesOld'] : [];

                $clock->thumbnails                              = $validatedData['thumbnails'];

                $clock->start_date                              = $validatedData['start_date'];
                $clock->end_date                                = $validatedData['end_date'];
                // Cập nhật sort order
//                $school->sort_order = $validatedData['sort_order'];

                // Tạo và lưu
//                $product = Product::create($payload);
//                DB::beginTransaction();

                // ƯU TIÊN CHẠY XÓA TRƯỚC ADD SAU
                if (! empty($payload['imagesOld'])) {
                    $clock->removeImages($payload['imagesOld']);
                }

                if (! empty($payload['images'])) {
                    // Cập nhật bucket path
                    $clock->addImages($payload['images']);
                }

                $clock->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage($payload['imagesOld']);
                $this->setStatusCode(200);
                $this->setData($clock);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Clock::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $clock = Clock::query()->get();
        foreach ($clock->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $clock = Clock::query()
            ->select([
                'id',
                'name',
                'slug',
                'price',
                'description',
                'thumbnails',
                'face_diameter',
                'type_machine',
                'wire_material',
                'frame_material',
                'face_thickness',
                'material_glass',
                'utilities',
                'waterproof',
                'wire_width',
                'replace_cord',
                'energy_sources',
                'battery_life',
                'subjects_used',
                'trademark',
                'manufacture',
                'start_date',
                'end_date'
            ])
            ->where('name', 'LIKE', "%$search%")
            ->orWhere('price', 'LIKE', "%$search%")
            ->orWhere('description', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($clock);
    }
}
