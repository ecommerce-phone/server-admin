<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\UserCreateRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends AbstractApiController
{
    public function index(Request $request)
    {
        $user = User::query()
            ->select([
                'id',
                'username',
                'password',
                'email',
                'mobile',
                'last_name',
                'identity_number',
                'first_name',
                'sex',
                'address',
                'role',
                'status',
            ])
            ->DataTablePaginate($request);

        return $this->item($user);
    }

    public function create(UserCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        $payload['username']                            = $validatedData['username'];
        $payload['password']                            = bcrypt($validatedData['password']);
        $payload['email']                               = $validatedData['email'];
        $payload['mobile']                              = $validatedData['mobile'];
        $payload['identity_number']                     = $validatedData['identity_number'];
        $payload['last_name']                           = $validatedData['last_name'];
        $payload['first_name']                          = $validatedData['first_name'];
        $payload['sex']                                 = $validatedData['sex'];
        $payload['address']                             = $validatedData['address'];
        $payload['role']                                = $validatedData['role'];
        $payload['status']                              = true;

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['username'])) {
            $this->setMessage('Đã tồn tại tên tài khoản');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $user = User::create($payload);
        DB::beginTransaction();

        try {
            $user->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm tài khoản thành công!');
            $this->setStatusCode(200);
            $this->setData($user);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return User::query()->findOrFail($id);
    }

    public function update(UserCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $user = User::query()->findOrFail($id);
        if (!$user) {
            $this->setMessage('Không có tài khoản này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $user->username                             = $validatedData['username'];
                $user->password                             = bcrypt($validatedData['password']);
                $user->email                                = $validatedData['email'];
                $user->mobile                               = $validatedData['mobile'];
                $user->identity_number                      = $validatedData['identity_number'];
                $user->last_name                            = $validatedData['last_name'];
                $user->first_name                           = $validatedData['first_name'];
                $user->sex                                  = $validatedData['sex'];
                $user->address                              = $validatedData['address'];
                $user->role                                 = $validatedData['role'];
                $user->status                               = $validatedData['status'];

                $user->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($user);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        User::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($title)
    {
        $user = User::query()->get();
        foreach ($user->pluck('name') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $user = User::query()
            ->select([
                'id',
                'username',
                'password',
                'email',
                'mobile',
                'last_name',
                'first_name',
                'identity_number',
                'sex',
                'address',
                'role',
                'status',
            ])
            ->where('username', 'LIKE', "%$search%")
            ->orWhere('email', 'LIKE', "%$search%")
            ->orWhere('mobile', 'LIKE', "%$search%")
            ->orWhere('last_name', 'LIKE', "%$search%")
            ->orWhere('first_name', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($user);
    }
}
