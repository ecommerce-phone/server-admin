<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\ProductCreateRequest;
use App\ProductImage;
use App\Product;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends AbstractApiController
{
    public function index(Request $request)
    {
        $product = Product::query()
            ->select([
                'id',
                'name',
                'slug',
                'price',
                'description',
                'thumbnails',
                'product_brand',
                'product_type',
                'screen',
                'screen_resolution',
                'wide_screen',
                'touch_screen',
                'back_camera_resolution',
                'video_quality',
                'flash',
                'advanced_photography',
                'front_camera_resolution',
                'video_call',
                'order_info',
                'operating_system',
                'cpu',
                'cpu_speed',
                'gpu',
                'ram',
                'internal_memory',
                'available_memory',
                'external_memory',
                'mobile_network',
                'sim',
                'wifi',
                'gps',
                'bluetooth',
                'charging_port',
                'headphone_jack',
                'other_connect',
                'design',
                'material',
                'size',
                'weight',
                'battery_capacity',
                'battery_type',
                'battery_technology',
                'advanced_security',
                'special_features',
                'recording',
                'radio',
                'watch_movie',
                'listening_music',
                'start_date',
                'end_date'
            ])
            ->DataTablePaginate($request);

        return $this->item($product);
    }

    public function create(ProductCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['name']                                    = $validatedData['name'];
        $payload['slug']                                    = $slugify->slugify($validatedData['name']);

        $payload['price']                                   = $validatedData['price'];
        $payload['description']                             = $validatedData['description'];

        $payload['product_brand']                           = $validatedData['product_brand'];
        $payload['product_type']                            = $validatedData['product_type'];
        $payload['screen']                                  = $validatedData['screen'];
        $payload['screen_resolution']                       = $validatedData['screen_resolution'];
        $payload['wide_screen']                             = $validatedData['wide_screen'];
        $payload['touch_screen']                            = $validatedData['touch_screen'];
        $payload['back_camera_resolution']                  = $validatedData['back_camera_resolution'];
        $payload['video_quality']                           = $validatedData['video_quality'];
        $payload['flash']                                   = $validatedData['flash'];

        // Hình ảnh
        $payload['images']                                  = ! empty($validatedData['images']) ? $validatedData['images'] : [];

        $payload['advanced_photography']                    = $validatedData['advanced_photography'];
        $payload['front_camera_resolution']                 = $validatedData['front_camera_resolution'];
        $payload['video_call']                              = $validatedData['video_call'];
        $payload['order_info']                              = $validatedData['order_info'];

        $payload['thumbnails']                              = $validatedData['thumbnails'];
        $payload['operating_system']                        = $validatedData['operating_system'];
        $payload['cpu']                                     = $validatedData['cpu'];
        $payload['cpu_speed']                               = $validatedData['cpu_speed'];
        $payload['gpu']                                     = $validatedData['gpu'];
        $payload['ram']                                     = $validatedData['ram'];

        $payload['internal_memory']                         = $validatedData['internal_memory'];
        $payload['available_memory']                        = $validatedData['available_memory'];
        $payload['external_memory']                         = $validatedData['external_memory'];
        $payload['mobile_network']                          = $validatedData['mobile_network'];
        $payload['sim']                                     = $validatedData['sim'];

        $payload['wifi']                                    = $validatedData['wifi'];
        $payload['gps']                                     = $validatedData['gps'];
        $payload['bluetooth']                               = $validatedData['bluetooth'];
        $payload['charging_port']                           = $validatedData['charging_port'];

        $payload['headphone_jack']                          = $validatedData['headphone_jack'];
        $payload['other_connect']                           = $validatedData['other_connect'];
        $payload['design']                                  = $validatedData['design'];
        $payload['material']                                = $validatedData['material'];
        $payload['size']                                    = $validatedData['size'];
        $payload['weight']                                  = $validatedData['weight'];
        $payload['battery_capacity']                        = $validatedData['battery_capacity'];
        $payload['battery_type']                            = $validatedData['battery_type'];
        $payload['battery_technology']                      = $validatedData['battery_technology'];
        $payload['advanced_security']                       = $validatedData['advanced_security'];
        $payload['special_features']                        = $validatedData['special_features'];
        $payload['recording']                               = $validatedData['recording'];
        $payload['radio']                                   = $validatedData['radio'];
        $payload['watch_movie']                             = $validatedData['watch_movie'];
        $payload['listening_music']                         = $validatedData['listening_music'];

        $payload['start_date']                              = $validatedData['start_date'];
        $payload['end_date']                                = $validatedData['end_date'];

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên sản phẩm');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $product = Product::create($payload);
        DB::beginTransaction();

        if (! empty($payload['images'])) {

//                foreach ($payload['images'] as $image_idx => $image) {
//                    $currentBucketPath = $image['filepath'];
//
//                    foreach ($image['uploaded_data'] as $thumb_index => $targetFile) {
//                        Storage::disk('minio')->copy(
//                            $currentBucketPath.'/'.$targetFile['filename'],
//                            $newBucketPath.'/'.$targetFile['filename']
//                        );
//
//                        $payload['images'][$image_idx]['filepath'] = $newBucketPath;
//                    }
//                }

            // Cập nhật bucket path
            $product->addImages($payload['images']);
        }

        try {
            $product->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm sản phẩm thành công!');
            $this->setStatusCode(200);
            $this->setData($product);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
//        return Post::query()->findOrFail($id);


//        $images = ProductImage::query()->where('post_id', '=', $id)->get();
//        $product = Post::query()->findOrFail($id);
//
//        $result = [
//            'images' => $images,
//            'post'     => $product,
//        ];
//
//        return $this->item($result);
        $query = Product::query();
        $query->where('id', '=', $id);
        $product = $query->firstOrFail();
        $product->load('images');

        return $this->item($product);
    }

    public function RemoveImagesOld($id)
    {
        return ProductImage::query()->where('id', '=', $id)->delete();
    }

    public function update(ProductCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $product = Product::query()->findOrFail($id);
        if (!$product) {
            $this->setMessage('Không có sản phẩm này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $product->name                                  = $validatedData['name'];
                $product->slug                                  = $slugify->slugify($validatedData['name']);
                $product->price                                 = $validatedData['price'];
                $product->description                           = $validatedData['description'];
                $product->product_brand                         = $validatedData['product_brand'];
                $product->product_type                          = $validatedData['product_type'];
                $product->screen                                = $validatedData['screen'];
                $product->screen_resolution                     = $validatedData['screen_resolution'];
                $product->wide_screen                           = $validatedData['wide_screen'];
                $product->touch_screen                          = $validatedData['touch_screen'];
                $product->back_camera_resolution                = $validatedData['back_camera_resolution'];
                $product->video_quality                         = $validatedData['video_quality'];
                $product->flash                                 = $validatedData['flash'];
                $product->advanced_photography                  = $validatedData['advanced_photography'];
                $product->front_camera_resolution               = $validatedData['front_camera_resolution'];
                $product->video_call                            = $validatedData['video_call'];
                $product->order_info                            = $validatedData['order_info'];
                $product->operating_system                      = $validatedData['operating_system'];
                $product->cpu                                   = $validatedData['cpu'];

                $payload['images']                              = ! empty($validatedData['images']) ? $validatedData['images'] : [];
                $payload['imagesOld']                           = ! empty($validatedData['imagesOld']) ? $validatedData['imagesOld'] : [];

                $product->thumbnails                            = $validatedData['thumbnails'];
                $product->cpu_speed                             = $validatedData['cpu_speed'];
                $product->gpu                                   = $validatedData['gpu'];
                $product->ram                                   = $validatedData['ram'];
                $product->internal_memory                       = $validatedData['internal_memory'];
                $product->available_memory                      = $validatedData['available_memory'];

                $product->external_memory                       = $validatedData['external_memory'];
                $product->mobile_network                        = $validatedData['mobile_network'];
                $product->sim                                   = $validatedData['sim'];
                $product->wifi                                  = $validatedData['wifi'];

                $product->gps                                   = $validatedData['gps'];
                $product->bluetooth                             = $validatedData['bluetooth'];
                $product->charging_port                         = $validatedData['charging_port'];
                $product->headphone_jack                        = $validatedData['headphone_jack'];
                $product->other_connect                         = $validatedData['other_connect'];
                $product->design                                = $validatedData['design'];
                $product->material                              = $validatedData['material'];
                $product->size                                  = $validatedData['size'];
                $product->weight                                = $validatedData['weight'];
                $product->battery_capacity                      = $validatedData['battery_capacity'];
                $product->battery_type                          = $validatedData['battery_type'];
                $product->battery_technology                    = $validatedData['battery_technology'];
                $product->advanced_security                     = $validatedData['advanced_security'];
                $product->special_features                      = $validatedData['special_features'];
                $product->recording                             = $validatedData['recording'];
                $product->radio                                 = $validatedData['radio'];
                $product->watch_movie                           = $validatedData['watch_movie'];
                $product->listening_music                       = $validatedData['listening_music'];
                $product->start_date                            = $validatedData['start_date'];
                $product->end_date                              = $validatedData['end_date'];
                // Cập nhật sort order
//                $school->sort_order = $validatedData['sort_order'];

                // Tạo và lưu
//                $product = Product::create($payload);
//                DB::beginTransaction();

                // ƯU TIÊN CHẠY XÓA TRƯỚC ADD SAU
                if (! empty($payload['imagesOld'])) {
                    $product->removeImages($payload['imagesOld']);
                }

                if (! empty($payload['images'])) {
                    // Cập nhật bucket path
                    $product->addImages($payload['images']);
                }

                $product->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage($payload['imagesOld']);
                $this->setStatusCode(200);
                $this->setData($product);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Product::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $product = Product::query()->get();
        foreach ($product->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $product = Product::query()
            ->select([
                'id',
                'name',
                'slug',
                'price',
                'description',
                'thumbnails',
                'product_brand',
                'product_type',
                'screen',
                'screen_resolution',
                'wide_screen',
                'touch_screen',
                'back_camera_resolution',
                'video_quality',
                'flash',
                'advanced_photography',
                'front_camera_resolution',
                'video_call',
                'order_info',
                'operating_system',
                'cpu',
                'cpu_speed',
                'gpu',
                'ram',
                'internal_memory',
                'available_memory',
                'external_memory',
                'mobile_network',
                'sim',
                'wifi',
                'gps',
                'bluetooth',
                'charging_port',
                'headphone_jack',
                'other_connect',
                'design',
                'material',
                'size',
                'weight',
                'battery_capacity',
                'battery_type',
                'battery_technology',
                'advanced_security',
                'special_features',
                'recording',
                'radio',
                'watch_movie',
                'listening_music',
                'start_date',
                'end_date'
            ])
            ->where('name', 'LIKE', "%$search%")
            ->orWhere('price', 'LIKE', "%$search%")
            ->orWhere('description', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($product);
    }
}
