<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends AbstractApiController
{
    public function index(Request $request)
    {
        $order = Order::query()
            ->select([
                'id',
                'product_id',
                'description',
                'product_count',
                'total_price',
                'order_date',
                'receive_date',
                'contact_name',
                'sex',
                'contact_mobile',
                'contact_address',
                'status',
            ])
            ->with('products')
            ->DataTablePaginate($request);

        return $this->item($order);
    }

    public function show($id)
    {
        return Order::query()->with('products')->findOrFail($id);
    }

    public function remove($id)
    {
        Order::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $order = Order::query()
            ->select([
                'id',
                'product_id',
                'description',
                'product_count',
                'total_price',
                'order_date',
                'receive_date',
                'contact_name',
                'sex',
                'contact_mobile',
                'contact_address',
                'status',
            ])
            ->with('products')
            ->where('contact_name', 'LIKE', "%$search%")
            ->orWhere('contact_mobile', 'LIKE', "%$search%")
            ->orWhere('description', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($order);
    }
}
