<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Accessory;
use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\AccessoryCreateRequest;
use App\ProductImage;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccessoryController extends AbstractApiController
{
    public function index(Request $request)
    {
        $accessory = Accessory::query()
            ->select([
                'id',
                'name',
                'slug',
                'price',
                'description',
                'thumbnails',
                'compatible',
                'charging_port',
                'used_time',
                'full_charge',
                'connect_same',
                'connection_distance',
                'weight',
                'trademark',
                'manufacture',
                'start_date',
                'end_date'
            ])
            ->DataTablePaginate($request);

        return $this->item($accessory);
    }

    public function create(AccessoryCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['name']                                    = $validatedData['name'];
        $payload['slug']                                    = $slugify->slugify($validatedData['name']);

        $payload['price']                                   = $validatedData['price'];
        $payload['description']                             = $validatedData['description'];

        $payload['compatible']                              = $validatedData['compatible'];
        $payload['charging_port']                           = $validatedData['charging_port'];
        $payload['used_time']                               = $validatedData['used_time'];
        $payload['full_charge']                             = $validatedData['full_charge'];
        $payload['connect_same']                            = $validatedData['connect_same'];
        $payload['connection_distance']                     = $validatedData['connection_distance'];
        $payload['weight']                                  = $validatedData['weight'];
        $payload['trademark']                               = $validatedData['trademark'];
        $payload['manufacture']                             = $validatedData['manufacture'];

        // Hình ảnh
        $payload['images']                                  = ! empty($validatedData['images']) ? $validatedData['images'] : [];

        $payload['thumbnails']                              = $validatedData['thumbnails'];

        $payload['start_date']                              = $validatedData['start_date'];
        $payload['end_date']                                = $validatedData['end_date'];

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên sản phẩm');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $accessory = Accessory::create($payload);
        DB::beginTransaction();

        if (! empty($payload['images'])) {

//                foreach ($payload['images'] as $image_idx => $image) {
//                    $currentBucketPath = $image['filepath'];
//
//                    foreach ($image['uploaded_data'] as $thumb_index => $targetFile) {
//                        Storage::disk('minio')->copy(
//                            $currentBucketPath.'/'.$targetFile['filename'],
//                            $newBucketPath.'/'.$targetFile['filename']
//                        );
//
//                        $payload['images'][$image_idx]['filepath'] = $newBucketPath;
//                    }
//                }

            // Cập nhật bucket path
            $accessory->addImages($payload['images']);
        }

        try {
            $accessory->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm sản phẩm thành công!');
            $this->setStatusCode(200);
            $this->setData($accessory);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
//        return Post::query()->findOrFail($id);


//        $images = ProductImage::query()->where('post_id', '=', $id)->get();
//        $product = Post::query()->findOrFail($id);
//
//        $result = [
//            'images' => $images,
//            'post'     => $product,
//        ];
//
//        return $this->item($result);
        $query = Accessory::query();
        $query->where('id', '=', $id);
        $product = $query->firstOrFail();
        $product->load('images');

        return $this->item($product);
    }

    public function RemoveImagesOld($id)
    {
        return ProductImage::query()->where('id', '=', $id)->delete();
    }

    public function update(AccessoryCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $accessory = Accessory::query()->findOrFail($id);
        if (!$accessory) {
            $this->setMessage('Không có sản phẩm này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $accessory->name                                    = $validatedData['name'];
                $accessory->slug                                    = $slugify->slugify($validatedData['name']);
                $accessory->price                                   = $validatedData['price'];
                $accessory->description                             = $validatedData['description'];

                $accessory->compatible                              = $validatedData['compatible'];
                $accessory->charging_port                           = $validatedData['charging_port'];
                $accessory->used_time                               = $validatedData['used_time'];
                $accessory->full_charge                             = $validatedData['full_charge'];
                $accessory->connect_same                            = $validatedData['connect_same'];
                $accessory->connection_distance                     = $validatedData['connection_distance'];
                $accessory->weight                                  = $validatedData['weight'];
                $accessory->trademark                               = $validatedData['trademark'];
                $accessory->manufacture                             = $validatedData['manufacture'];

                $payload['images']                                  = ! empty($validatedData['images']) ? $validatedData['images'] : [];
                $payload['imagesOld']                               = ! empty($validatedData['imagesOld']) ? $validatedData['imagesOld'] : [];

                $accessory->thumbnails                              = $validatedData['thumbnails'];

                $accessory->start_date                              = $validatedData['start_date'];
                $accessory->end_date                                = $validatedData['end_date'];
                // Cập nhật sort order
//                $school->sort_order = $validatedData['sort_order'];

                // Tạo và lưu
//                $product = Product::create($payload);
//                DB::beginTransaction();

                // ƯU TIÊN CHẠY XÓA TRƯỚC ADD SAU
                if (! empty($payload['imagesOld'])) {
                    $accessory->removeImages($payload['imagesOld']);
                }

                if (! empty($payload['images'])) {
                    // Cập nhật bucket path
                    $accessory->addImages($payload['images']);
                }

                $accessory->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage($payload['imagesOld']);
                $this->setStatusCode(200);
                $this->setData($accessory);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Accessory::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $accessory = Accessory::query()->get();
        foreach ($accessory->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $accessory = Accessory::query()
            ->select([
                'id',
                'name',
                'slug',
                'price',
                'description',
                'thumbnails',
                'compatible',
                'charging_port',
                'used_time',
                'full_charge',
                'connect_same',
                'connection_distance',
                'weight',
                'trademark',
                'manufacture',
                'start_date',
                'end_date'
            ])
            ->where('name', 'LIKE', "%$search%")
            ->orWhere('price', 'LIKE', "%$search%")
            ->orWhere('description', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($accessory);
    }
}
