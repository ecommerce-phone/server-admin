<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Comment;
use App\Http\Controllers\AbstractApiController;

use Illuminate\Http\Request;

class CommentController extends AbstractApiController
{
    public function index(Request $request)
    {
        $comment = Comment::query()
            ->select([
                'id',
                'product_id',
                'contact_name',
                'contact_address',
                'contact_mobile',
                'contact_email',
                'sex',
                'question',
            ])
            ->DataTablePaginate($request);

        return $this->item($comment);
    }


    public function remove($id)
    {
        Comment::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $comment = Comment::query()
            ->select([
                'id',
                'product_id',
                'contact_name',
                'contact_address',
                'contact_mobile',
                'contact_email',
                'sex',
                'question',
            ])
            ->where('product_id', 'LIKE', "%$search%")
            ->orWhere('contact_name', 'LIKE', "%$search%")
            ->orWhere('contact_address', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($comment);
    }
}
