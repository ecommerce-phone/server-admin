<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccessoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                                              => 'required',
            'price'                                             => 'required',
            'description'                                       => 'required',
            'thumbnails'                                        => 'required',
            'compatible'                                        => 'required',
            'charging_port'                                     => 'required',
            'used_time'                                         => 'required',
            'full_charge'                                       => 'required',
            'connect_same'                                      => 'required',
            'connection_distance'                               => 'required',
            'weight'                                            => 'required',
            'trademark'                                         => 'required',
            'manufacture'                                       => 'required',

            // Xóa ảnh cũ
            'imagesOld.*'                                       => 'required',
            // Thư viện ảnh
            'images.*.filepath'                                 => 'required',
            'images.*.type'                                     => 'required',
            'images.*.sort_order'                               => 'required|numeric',
            'images.*.uploaded_data.*.ranting'                  => 'required',
            'images.*.uploaded_data.*.filename'                 => 'required',
            'images.*.uploaded_data.*.width'                    => 'required|numeric',
            'images.*.uploaded_data.*.height'                   => 'required|numeric',

            'start_date'                                        => 'required',
            'end_date'                                          => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'                                     => 'Bạn chưa nhập tên sản phẩm',
            'price.required'                                    => 'Bạn chưa nhập giá bán',
            'description.required'                              => 'Bạn chưa nhập mô tả',
            'thumbnails.required'                               => 'Bạn chưa nhập ảnh đại diện',
            'compatible.required'                               => 'Bạn chưa nhập tương thích',
            'charging_port.required'                            => 'Bạn chưa nhập cổng sạc',
            'used_time.required'                                => 'Bạn chưa nhập thời gian sử dụng',
            'full_charge.required'                              => 'Bạn chưa nhập thời gian sạc đầy',
            'connect_same.required'                             => 'Bạn chưa nhập kết nối cùng lúc',
            'connection_distance.required'                      => 'Bạn chưa nhập khoảng cách kết nối',
            'weight.required'                                   => 'Bạn chưa nhập trọng lượng',
            'trademark.required'                                => 'Bạn chưa nhập thương hiệu',
            'manufacture.required'                              => 'Bạn chưa nhập nơi sản xuất',

            'start_date.required'                               => 'Bạn chưa nhập ngày bắt đầu đăng tin',
            'end_date.required'                                 => 'Bạn chưa nhập ngày kết thúc đăng tin',
        ];
    }
}
