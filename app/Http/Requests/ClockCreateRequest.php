<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClockCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                                              => 'required',
            'price'                                             => 'required',
            'description'                                       => 'required',
            'thumbnails'                                        => 'required',
            'face_diameter'                                     => 'required',
            'type_machine'                                      => 'required',
            'wire_material'                                     => 'required',
            'frame_material'                                    => 'required',
            'face_thickness'                                    => 'required',
            'material_glass'                                    => 'required',
            'utilities'                                         => 'required',
            'waterproof'                                        => 'required',
            'wire_width'                                        => 'required',
            'replace_cord'                                      => 'required',
            'energy_sources'                                    => 'required',
            'battery_life'                                      => 'required',
            'subjects_used'                                     => 'required',
            'trademark'                                         => 'required',
            'manufacture'                                       => 'required',

            // Xóa ảnh cũ
            'imagesOld.*'                                       => 'required',
            // Thư viện ảnh
            'images.*.filepath'                                 => 'required',
            'images.*.type'                                     => 'required',
            'images.*.sort_order'                               => 'required|numeric',
            'images.*.uploaded_data.*.ranting'                  => 'required',
            'images.*.uploaded_data.*.filename'                 => 'required',
            'images.*.uploaded_data.*.width'                    => 'required|numeric',
            'images.*.uploaded_data.*.height'                   => 'required|numeric',

            'start_date'                                        => 'required',
            'end_date'                                          => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'                                     => 'Bạn chưa nhập tên sản phẩm',
            'price.required'                                    => 'Bạn chưa nhập giá bán',
            'description.required'                              => 'Bạn chưa nhập mô tả',
            'thumbnails.required'                               => 'Bạn chưa nhập ảnh đại diện',
            'face_diameter.required'                            => 'Bạn chưa nhập đường kính mặt',
            'type_machine.required'                             => 'Bạn chưa nhập loại máy',
            'wire_material.required'                            => 'Bạn chưa nhập chất liệu dây',
            'frame_material.required'                           => 'Bạn chưa nhập chất liệu khung viền',
            'face_thickness.required'                           => 'Bạn chưa nhập độ dày mặt',
            'material_glass.required'                           => 'Bạn chưa nhập chất liệu mặt kính',
            'utilities.required'                                => 'Bạn chưa nhập tiện ích',
            'waterproof.required'                               => 'Bạn chưa nhập chống nước',
            'wire_width.required'                               => 'Bạn chưa nhập độ rộng dây',
            'replace_cord.required'                             => 'Bạn chưa nhập thay được dây',
            'energy_sources.required'                           => 'Bạn chưa nhập nguồn năng lượng',
            'battery_life.required'                             => 'Bạn chưa nhập thời gian sử dụng PIN',
            'subjects_used.required'                            => 'Bạn chưa nhập dối tượng sử dụng',
            'trademark.required'                                => 'Bạn chưa nhập thương hiệu',
            'manufacture.required'                              => 'Bạn chưa nhập nơi sản xuất',

            'start_date.required'                               => 'Bạn chưa nhập ngày bắt đầu đăng tin',
            'end_date.required'                                 => 'Bạn chưa nhập ngày kết thúc đăng tin',
        ];
    }
}
