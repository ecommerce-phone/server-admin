<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'                                             => 'required',
            'content'                                           => 'required',
            'content_type'                                      => 'required',
            'thumbnails'                                        => 'required',
            'source'                                            => 'required',
            'start_date'                                        => 'required',
            'end_date'                                          => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'                                    => 'Bạn chưa nhập tiêu đề',
            'content.required'                                  => 'Bạn chưa nhập nội dung mô tả',
            'content_type.required'                             => 'Bạn chưa nhập loại nội dung',
            'thumbnails.required'                               => 'Bạn chưa nhập ảnh đại diện',
            'source.required'                                   => 'Bạn chưa nhập nguồn tin',

            'start_date.required'                               => 'Bạn chưa nhập ngày bắt đầu đăng tin',
            'end_date.required'                                 => 'Bạn chưa nhập ngày kết thúc đăng tin',
        ];
    }
}
