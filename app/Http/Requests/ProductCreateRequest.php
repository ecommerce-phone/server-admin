<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                                              => 'required',
            'price'                                             => 'required',
            'description'                                       => 'required',
            'thumbnails'                                        => 'required',
            'product_brand'                                     => 'required',
            'product_type'                                      => 'required',
            'screen'                                            => 'required',
            'screen_resolution'                                 => 'required',
            'wide_screen'                                       => 'required',
            'touch_screen'                                      => 'required',
            'back_camera_resolution'                            => 'required',
            'video_quality'                                     => 'required',
            'flash'                                             => 'required',
            'advanced_photography'                              => 'required',
            'front_camera_resolution'                           => 'required',
            'video_call'                                        => 'required',

            // Xóa ảnh cũ
            'imagesOld.*'                                       => 'required',
            // Thư viện ảnh
            'images.*.filepath'                                 => 'required',
            'images.*.type'                                     => 'required',
            'images.*.sort_order'                               => 'required|numeric',
            'images.*.uploaded_data.*.ranting'                  => 'required',
            'images.*.uploaded_data.*.filename'                 => 'required',
            'images.*.uploaded_data.*.width'                    => 'required|numeric',
            'images.*.uploaded_data.*.height'                   => 'required|numeric',

            'order_info'                                        => 'required',
            'operating_system'                                  => 'required',
            'cpu'                                               => 'required',
            'cpu_speed'                                         => 'required',
            'gpu'                                               => 'required',

            'ram'                                               => 'required',
            'internal_memory'                                   => 'required',
            'available_memory'                                  => 'required',
            'external_memory'                                   => 'required',
            'mobile_network'                                    => 'required',

            'sim'                                               => 'required',
            'wifi'                                              => 'required',
            'gps'                                               => 'required',
            'bluetooth'                                         => 'required',
            'charging_port'                                     => 'required',
            'headphone_jack'                                    => 'required',
            'other_connect'                                     => 'required',
            'design'                                            => 'required',
            'material'                                          => 'required',
            'size'                                              => 'required',
            'weight'                                            => 'required',
            'battery_capacity'                                  => 'required',
            'battery_type'                                      => 'required',
            'battery_technology'                                => 'required',
            'advanced_security'                                 => 'required',
            'special_features'                                  => 'required',
            'recording'                                         => 'required',
            'radio'                                             => 'required',
            'watch_movie'                                       => 'required',
            'listening_music'                                   => 'required',
            'start_date'                                        => 'required',
            'end_date'                                          => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'                                     => 'Bạn chưa nhập tên sản phẩm',
            'price.required'                                    => 'Bạn chưa nhập giá bán',
            'description.required'                              => 'Bạn chưa nhập mô tả',
            'thumbnails.required'                               => 'Bạn chưa nhập ảnh đại diện',
            'product_brand.required'                            => 'Bạn chưa nhập nhãn hiệu',
            'product_type.required'                             => 'Bạn chưa nhập loại sản phẩm',
            'screen.required'                                   => 'Bạn chưa nhập màn hình',
            'screen_resolution.required'                        => 'Bạn chưa nhập độ phân giải',
            'wide_screen.required'                              => 'Bạn chưa nhập màn hình rộng',
            'touch_screen.required'                             => 'Bạn chưa nhập màn hình cảm ứng',
            'back_camera_resolution.required'                   => 'Bạn chưa nhập độ Phân giải camera sau',
            'video_quality.required'                            => 'Bạn chưa nhập chất lượng video',
            'flash.required'                                    => 'Bạn chưa nhập đèn flash',
            'advanced_photography.required'                     => 'Bạn chưa nhập chụp ảnh nâng cao',
            'front_camera_resolution.required'                  => 'Bạn chưa nhập độ Phân giải camera trước',
            'video_call.required'                               => 'Bạn chưa nhập video gọi thoại',
            'order_info.required'                               => 'Bạn chưa nhập thông tin khác',
            'operating_system.required'                         => 'Bạn chưa nhập hệ điều hành',
            'cpu.required'                                      => 'Bạn chưa nhập CPU',
            'cpu_speed.required'                                => 'Bạn chưa nhập tốc độ CPU',
            'gpu.required'                                      => 'Bạn chưa nhập chip đồ họa',

            'ram.required'                                      => 'Bạn chưa nhập ram',
            'internal_memory.required'                          => 'Bạn chưa nhập bộ nhớ trong',
            'available_memory.required'                         => 'Bạn chưa nhập bộ nhớ khả dụng',
            'external_memory.required'                          => 'Bạn chưa nhập thẻ nhớ ngoài',
            'mobile_network.required'                           => 'Bạn chưa nhập mạng di động',

            'sim.required'                                      => 'Bạn chưa nhập thẻ SIM',
            'wifi.required'                                     => 'Bạn chưa nhập WIFI',
            'gps.required'                                      => 'Bạn chưa nhập GPS',
            'bluetooth.required'                                => 'Bạn chưa nhập BLUETOOTH',
            'charging_port.required'                            => 'Bạn chưa nhập cổng kết nối/sạc',
            'headphone_jack.required'                           => 'Bạn chưa nhập jack tai nghe',
            'other_connect.required'                            => 'Bạn chưa nhập kết nối khác',
            'design.required'                                   => 'Bạn chưa nhập thiết kế',
            'material.required'                                 => 'Bạn chưa nhập chất liệu',
            'size.required'                                     => 'Bạn chưa nhập kích thước',
            'weight.required'                                   => 'Bạn chưa nhập trọng lượng',
            'battery_capacity.required'                         => 'Bạn chưa nhập dung lượng PIN',
            'battery_type.required'                             => 'Bạn chưa nhập loại PIN',
            'battery_technology.required'                       => 'Bạn chưa nhập công nghệ PIN',
            'advanced_security.required'                        => 'Bạn chưa nhập bảo mật nâng cao',
            'special_features.required'                         => 'Bạn chưa nhập tính năng đặc biệt',
            'recording.required'                                => 'Bạn chưa nhập ghi âm',
            'radio.required'                                    => 'Bạn chưa nhập radio',
            'watch_movie.required'                              => 'Bạn chưa nhập xem phim',
            'listening_music.required'                          => 'Bạn chưa nhập nghe nhạc',
            'start_date.required'                               => 'Bạn chưa nhập ngày bắt đầu đăng tin',
            'end_date.required'                                 => 'Bạn chưa nhập ngày kết thúc đăng tin',
        ];
    }
}
