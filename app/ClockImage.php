<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClockImage extends Model
{
    protected  $table = "clock_images";

    protected $fillable = [
        'clock_id',
        'filepath',
        'type',
        'o',
        'xs',
        'uploaded_data',
        'sort_order',
    ];

    protected $casts = [
        'uploaded_data' => 'array',
    ];
}
