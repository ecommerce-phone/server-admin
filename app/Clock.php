<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Clock extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'name',
        'slug',
        'price',
        'description',
        'thumbnails',
        'face_diameter',
        'type_machine',
        'wire_material',
        'frame_material',
        'face_thickness',
        'material_glass',
        'utilities',
        'waterproof',
        'wire_width',
        'replace_cord',
        'energy_sources',
        'battery_life',
        'subjects_used',
        'trademark',
        'manufacture',
        'start_date',
        'end_date'
    ];

    protected $filter = [
        'id',
        'name',
        'slug',
        'price',
        'description',
        'thumbnails',
        'face_diameter',
        'type_machine',
        'wire_material',
        'frame_material',
        'face_thickness',
        'material_glass',
        'utilities',
        'waterproof',
        'wire_width',
        'replace_cord',
        'energy_sources',
        'battery_life',
        'subjects_used',
        'trademark',
        'manufacture',
        'start_date',
        'end_date'
    ];

    // Test RemoveImagesOld
    public function removeImages($imagesOld)
    {
        $query = ClockImage::query();
        foreach ($imagesOld as $idImages) {
            $query->where('clock_id', '=', $this->id)
                ->where('id', '!=', $idImages);
        }
        $query->delete();
    }

    /**
     * Thêm hình ảnh vào bài đăng
     *
     * @param  mixed  $images
     */

    public function addImages($images)
    {
        $has_image     = false;
        $has_image_360 = false;

        foreach ($images as $imageObj) {
            $d = [
                'clock_id' => $this->id,

                'filepath'      => $imageObj['filepath'],
                'type'          => $imageObj['type'],
                'o'             => null,
                'xs'            => null,
                'uploaded_data' => $imageObj['uploaded_data'],
                'sort_order'    => $imageObj['sort_order'],
            ];

            foreach ($imageObj['uploaded_data'] as $image) {
                if ($image['ranting'] == 'o') {
//                    $d['o'] = $imageObj['filepath'].'/'.$image['filename'];
                    $d['o'] = $image['filename'];
                }

                if ($image['ranting'] == 'xs') {
//                    $d['xs'] = $imageObj['filepath'].'/'.$image['filename'];
                    $d['xs'] = $image['filename'];
                }
            }

            if ($imageObj['type'] == 'image') {
                $has_image = true;
            } elseif ($imageObj['type'] == 'image_360') {
                $has_image_360 = true;
            }

            ClockImage::create($d);
        }

        $this->has_image     = $has_image;
        $this->has_image_360 = $has_image_360;
    }

    /**
     * Thay thế ảnh của bài đăng
     *
     * @param  mixed  $images
     *
     * @throws \Exception
     */
    public function setImages($images)
    {
        ClockImage::where('clock_id', '=', $this->id)->delete();

        $this->addImages($images);
    }

    public function images()
    {
        return $this->hasMany(ClockImage::class, 'clock_id', 'id');
    }
}
