<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'product_id',
        'contact_name',
        'contact_address',
        'contact_mobile',
        'contact_email',
        'sex',
        'question',
    ];

    protected $filter = [
        'id',
        'product_id',
        'contact_name',
        'contact_address',
        'contact_mobile',
        'contact_email',
        'sex',
        'question',
    ];
}
