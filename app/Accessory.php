<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Accessory extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'name',
        'slug',
        'price',
        'description',
        'thumbnails',
        'compatible',
        'charging_port',
        'used_time',
        'full_charge',
        'connect_same',
        'connection_distance',
        'weight',
        'trademark',
        'manufacture',
        'start_date',
        'end_date'
    ];

    protected $filter = [
        'id',
        'name',
        'slug',
        'price',
        'description',
        'thumbnails',
        'compatible',
        'charging_port',
        'used_time',
        'full_charge',
        'connect_same',
        'connection_distance',
        'weight',
        'trademark',
        'manufacture',
        'start_date',
        'end_date'
    ];

    // Test RemoveImagesOld
    public function removeImages($imagesOld)
    {
        $query = AccessoryImage::query();
        foreach ($imagesOld as $idImages) {
            $query->where('accessory_id', '=', $this->id)
                ->where('id', '!=', $idImages);
        }
        $query->delete();
    }

    /**
     * Thêm hình ảnh vào bài đăng
     *
     * @param  mixed  $images
     */

    public function addImages($images)
    {
        $has_image     = false;
        $has_image_360 = false;

        foreach ($images as $imageObj) {
            $d = [
                'accessory_id' => $this->id,

                'filepath'      => $imageObj['filepath'],
                'type'          => $imageObj['type'],
                'o'             => null,
                'xs'            => null,
                'uploaded_data' => $imageObj['uploaded_data'],
                'sort_order'    => $imageObj['sort_order'],
            ];

            foreach ($imageObj['uploaded_data'] as $image) {
                if ($image['ranting'] == 'o') {
//                    $d['o'] = $imageObj['filepath'].'/'.$image['filename'];
                    $d['o'] = $image['filename'];
                }

                if ($image['ranting'] == 'xs') {
//                    $d['xs'] = $imageObj['filepath'].'/'.$image['filename'];
                    $d['xs'] = $image['filename'];
                }
            }

            if ($imageObj['type'] == 'image') {
                $has_image = true;
            } elseif ($imageObj['type'] == 'image_360') {
                $has_image_360 = true;
            }

            AccessoryImage::create($d);
        }

        $this->has_image     = $has_image;
        $this->has_image_360 = $has_image_360;
    }

    /**
     * Thay thế ảnh của bài đăng
     *
     * @param  mixed  $images
     *
     * @throws \Exception
     */
    public function setImages($images)
    {
        AccessoryImage::where('accessory_id', '=', $this->id)->delete();

        $this->addImages($images);
    }

    public function images()
    {
        return $this->hasMany(AccessoryImage::class, 'accessory_id', 'id');
    }
}
