<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = [
        'product_id',
        'filepath',
        'type',
        'o',
        'xs',
        'uploaded_data',
        'sort_order',
    ];

    protected $casts = [
        'uploaded_data' => 'array',
    ];
}
