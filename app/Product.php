<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'name',
        'slug',
        'price',
        'description',
        'thumbnails',
        'product_brand',
        'product_type',
        'screen',
        'screen_resolution',
        'wide_screen',
        'touch_screen',
        'back_camera_resolution',
        'video_quality',
        'flash',
        'advanced_photography',
        'front_camera_resolution',
        'video_call',
        'order_info',
        'operating_system',
        'cpu',
        'cpu_speed',
        'gpu',
        'ram',
        'internal_memory',
        'available_memory',
        'external_memory',
        'mobile_network',
        'sim',
        'wifi',
        'gps',
        'bluetooth',
        'charging_port',
        'headphone_jack',
        'other_connect',
        'design',
        'material',
        'size',
        'weight',
        'battery_capacity',
        'battery_type',
        'battery_technology',
        'advanced_security',
        'special_features',
        'recording',
        'radio',
        'watch_movie',
        'listening_music',
        'start_date',
        'end_date'
    ];

    protected $filter = [
        'id',
        'name',
        'slug',
        'price',
        'description',
        'thumbnails',
        'product_brand',
        'product_type',
        'screen',
        'screen_resolution',
        'wide_screen',
        'touch_screen',
        'back_camera_resolution',
        'video_quality',
        'flash',
        'advanced_photography',
        'front_camera_resolution',
        'video_call',
        'order_info',
        'operating_system',
        'cpu',
        'cpu_speed',
        'gpu',
        'ram',
        'internal_memory',
        'available_memory',
        'external_memory',
        'mobile_network',
        'sim',
        'wifi',
        'gps',
        'bluetooth',
        'charging_port',
        'headphone_jack',
        'other_connect',
        'design',
        'material',
        'size',
        'weight',
        'battery_capacity',
        'battery_type',
        'battery_technology',
        'advanced_security',
        'special_features',
        'recording',
        'radio',
        'watch_movie',
        'listening_music',
        'start_date',
        'end_date'
    ];

    // Test RemoveImagesOld
    public function removeImages($imagesOld)
    {
        $query = ProductImage::query();
        foreach ($imagesOld as $idImages) {
            $query->where('product_id', '=', $this->id)
                ->where('id', '!=', $idImages);
        }
        $query->delete();
    }

    /**
     * Thêm hình ảnh vào bài đăng
     *
     * @param  mixed  $images
     */

    public function addImages($images)
    {
        $has_image     = false;
        $has_image_360 = false;

        foreach ($images as $imageObj) {
            $d = [
                'product_id' => $this->id,

                'filepath'      => $imageObj['filepath'],
                'type'          => $imageObj['type'],
                'o'             => null,
                'xs'            => null,
                'uploaded_data' => $imageObj['uploaded_data'],
                'sort_order'    => $imageObj['sort_order'],
            ];

            foreach ($imageObj['uploaded_data'] as $image) {
                if ($image['ranting'] == 'o') {
//                    $d['o'] = $imageObj['filepath'].'/'.$image['filename'];
                    $d['o'] = $image['filename'];
                }

                if ($image['ranting'] == 'xs') {
//                    $d['xs'] = $imageObj['filepath'].'/'.$image['filename'];
                    $d['xs'] = $image['filename'];
                }
            }

            if ($imageObj['type'] == 'image') {
                $has_image = true;
            } elseif ($imageObj['type'] == 'image_360') {
                $has_image_360 = true;
            }

            ProductImage::create($d);
        }

        $this->has_image     = $has_image;
        $this->has_image_360 = $has_image_360;
    }

    /**
     * Thay thế ảnh của bài đăng
     *
     * @param  mixed  $images
     *
     * @throws \Exception
     */
    public function setImages($images)
    {
        ProductImage::where('product_id', '=', $this->id)->delete();

        $this->addImages($images);
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class, 'product_id', 'id');
    }
}
