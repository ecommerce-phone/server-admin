<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, DataTablePaginate, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'username',
        'password',
        'email',
        'mobile',
        'last_name',
        'first_name',
        'identity_number',
        'sex',
        'address',
        'role',
        'status',
    ];

    protected $filter = [
        'id',
        'username',
        'password',
        'email',
        'mobile',
        'last_name',
        'first_name',
        'identity_number',
        'sex',
        'address',
        'role',
        'status',
    ];

    /**
     * Find the user instance for the given username.
     *
     * @param  string  $username
     * @return \App\User
     */
    public function findForPassport($username)
    {
        return $this->where('username', $username)->first();
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
