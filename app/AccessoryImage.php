<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessoryImage extends Model
{
    protected  $table = "accessory_images";

    protected $fillable = [
        'accessory_id',
        'filepath',
        'type',
        'o',
        'xs',
        'uploaded_data',
        'sort_order',
    ];

    protected $casts = [
        'uploaded_data' => 'array',
    ];
}
