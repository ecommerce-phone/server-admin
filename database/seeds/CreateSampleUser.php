<?php

use Illuminate\Database\Seeder;
use App\User;

class CreateSampleUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'username' => 'demoad',
                'password' => bcrypt('123456'),
                'email' => 'Administrator',
                'mobile' => '0937519106',
                'last_name' => 'Nguyễn',
                'first_name' => 'Tấn Thành',
                'identity_number' => '123456789',
                'sex' => 1,
                'address' => "Bình Dương",
                'role' => 1
            ]
        );
    }
}
