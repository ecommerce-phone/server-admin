<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique()->comment('Tên tài khoản');
            $table->string('password')->comment('Mật khẩu, đã băm bằng bcrypt');
            $table->string('email')->unique()->comment('Email');

            $table->string('mobile')->unique()->comment('Số điện thoại di động');

            // Thông tin cá nhân
            $table->string('identity_number')->default('')->comment('CMND/Thẻ căn cước');
            $table->string('last_name')->comment('Họ');
            $table->string('first_name')->comment('Tên');
            $table->tinyInteger('sex')->default(1)->comment('Giới tính 0 = nữ, 1 = nam');

            $table->string('address')->default('')->comment('Địa chỉ liên hệ');

            // Quyền hạn
            $table->tinyInteger('role')->default(0)->comment('Loại tài khoản,0 = khách, 1 = admin');
            $table->boolean('status')->default(true)->comment('Tình trạng khóa tài khoản');

            // Tạo code để reset password
            $table->string('code')->nullable()->index();
            $table->timestamp('time_code')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
