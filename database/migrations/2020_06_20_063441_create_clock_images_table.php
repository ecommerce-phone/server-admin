<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClockImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clock_images', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('clock_id')->comment('Mã sản phẩm, khóa ngoại');

            $table->string('filepath')->comment('CDN bucket path');
            $table->string('type')->comment('Loại hình image, image_360');
            $table->string('o')->comment('Hình chất lượng cao');
            $table->string('xs')->comment('Hình thu nhỏ');
            $table->json('uploaded_data')->comment('JSON data cho hình với nhiều chất lượng khác nhau');
            $table->unsignedTinyInteger('sort_order')->default(0)->comment('Độ ưu tiên');

            $table->index(['clock_id']);

            $table->foreign('clock_id')
                ->references('id')->on('clocks')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clock_images');
    }
}
