<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();

            // Thông tin cơ bản
            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');
            $table->string('name')->default('')->comment('Tên sản phẩm');
            $table->double('price')->default(0)->comment('Giá sản phẩm');
            $table->longText('description')->comment('Mô tả sản phẩm');
            $table->unsignedInteger('product_brand')->comment('Nhãn hiệu: Iphone,Samsung,...');
            $table->unsignedInteger('product_type')->comment('Loại sản phẩm: Điện thoại, Ipad');

            // Cấu hình chi tiết
            // Hình ảnh, video và vị trí
            $table->boolean('has_image')->default(false)->comment('Bài đăng có hình');
            $table->boolean('has_image_360')->default(false)->comment('Bài đăng có hình 360');
            $table->string('thumbnails')->default('')->comment('Hình đại diện');

            // Màn hình
            $table->string('screen')->default('')->comment('Màn hình');
            $table->string('screen_resolution')->default('')->comment('Độ phân giải');
            $table->string('wide_screen')->default('')->comment('Màn hình rộng');
            $table->string('touch_screen')->default('')->comment('Màn kính cảm ứng');

            // Camera sau
            $table->string('back_camera_resolution')->default('')->comment('Độ Phân giải camera sau');
            $table->string('video_quality')->default('')->comment('Chất lượng Video');
            $table->string('flash')->default('')->comment('Đèn Flash');
            $table->string('advanced_photography')->default('')->comment('Chụp ảnh nâng cao');

            // Camera trước
            $table->string('front_camera_resolution')->default('')->comment('Độ Phân giải camera trước');
            $table->string('video_call')->default('')->comment('Video gọi thoại');
            $table->string('order_info')->default('')->comment('Thông tin khác');

            // Hệ điều hành CPU
            $table->string('operating_system')->default('')->comment('Hệ điều hành');
            $table->string('cpu')->default('')->comment('CPU');
            $table->string('cpu_speed')->default('')->comment('Tốc độ CPU');
            $table->string('gpu')->default('')->comment('Chip đồ họa');

            // Bộ nhớ và lưu trữ
            $table->string('ram')->default('')->comment('RAM');
            $table->string('internal_memory')->default('')->comment('Bộ nhớ trong');
            $table->string('available_memory')->default('')->comment('Bộ nhớ khả dụng');
            $table->string('external_memory')->default('')->comment('Thẻ nhớ ngoài');

            // Kết nối
            $table->string('mobile_network')->default('')->comment('Mạng di động');
            $table->string('sim')->default('')->comment('Thẻ sim');
            $table->string('wifi')->default('')->comment('Wifi');
            $table->string('gps')->default('')->comment('GPS');
            $table->string('bluetooth')->default('')->comment('Bluetooth');
            $table->string('charging_port')->default('')->comment('Cổng kết nối/sạc');
            $table->string('headphone_jack')->default('')->comment('Jack tai nghe');
            $table->string('other_connect')->default('')->comment('Kết nối khác');

            // Thiết kế và trọng lượng
            $table->string('design')->default('')->comment('Thiết kế');
            $table->string('material')->default('')->comment('Chất liệu');
            $table->string('size')->default('')->comment('Kích thước');
            $table->string('weight')->default('')->comment('Trọng lượng');

            // Thông tin và sạc
            $table->string('battery_capacity')->default('')->comment('Dung lượng PIN');
            $table->string('battery_type')->default('')->comment('Loại PIN');
            $table->string('battery_technology')->default('')->comment('Công nghệ PIN');

            // Tiện ích
            $table->string('advanced_security')->default('')->comment('Bảo mật nâng cao');
            $table->string('special_features')->default('')->comment('Tính năng đặc biệt');
            $table->string('recording')->default('')->comment('Ghi âm');
            $table->string('radio')->default('')->comment('Radio');
            $table->string('watch_movie')->default('')->comment('Xem phim');
            $table->string('listening_music')->default('')->comment('Nghe nhạc');

            // Thời gian tồn tại
            $table->date('start_date')->nullable()->comment('Ngày bắt đầu');
            $table->date('end_date')->nullable()->comment('Ngày kết thúc');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
