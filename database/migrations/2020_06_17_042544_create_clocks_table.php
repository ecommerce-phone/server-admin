<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clocks', function (Blueprint $table) {
            $table->id();

            // Thông tin cơ bản
            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');
            $table->string('name')->default('')->comment('Tên sản phẩm');
            $table->string('thumbnails')->default('')->comment('Màn hình');
            $table->double('price')->default(0)->comment('Giá sản phẩm');
            $table->longText('description')->comment('Mô tả sản phẩm');

            // Cấu hình chi tiết
            $table->boolean('has_image')->default(false)->comment('Bài đăng có hình');
            $table->boolean('has_image_360')->default(false)->comment('Bài đăng có hình 360');

            $table->string('face_diameter')->default('')->comment('Đường kính mặt');
            $table->string('type_machine')->default('')->comment('Loại máy');
            $table->string('wire_material')->default('')->comment('Chất liệu dây');
            $table->string('frame_material')->default('')->comment('Chất liệu khung viền');

            $table->string('face_thickness')->default('')->comment('Độ dày mặt');
            $table->string('material_glass')->default('')->comment('Chất liệu mặt kính');
            $table->string('utilities')->default('')->comment('Tiện ích');
            $table->string('waterproof')->default('')->comment('Chống nước');

            $table->string('wire_width')->default('')->comment('Độ rộng dây');
            $table->string('replace_cord')->default('')->comment('Thay được dây');
            $table->string('energy_sources')->default('')->comment('Nguồn năng lượng');

            $table->string('battery_life')->default('')->comment('Thời gian sử dụng pin');
            $table->string('subjects_used')->default('')->comment('Đối tượng sử dụng');
            $table->string('trademark')->default('')->comment('Thương hiệu');
            $table->string('manufacture')->default('')->comment('Nơi sản xuất (tùy từng lô hàng)');

            // Thời gian tồn tại
            $table->date('start_date')->nullable()->comment('Ngày bắt đầu');
            $table->date('end_date')->nullable()->comment('Ngày kết thúc');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clocks');
    }
}
