<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accessories', function (Blueprint $table) {
            $table->id();

            // Thông tin cơ bản
            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');
            $table->string('name')->default('')->comment('Tên sản phẩm');
            $table->string('thumbnails')->default('')->comment('Màn hình');
            $table->double('price')->default(0)->comment('Giá sản phẩm');
            $table->longText('description')->comment('Mô tả sản phẩm');

            // Cấu hình chi tiết
            $table->boolean('has_image')->default(false)->comment('Bài đăng có hình');
            $table->boolean('has_image_360')->default(false)->comment('Bài đăng có hình 360');

            $table->string('compatible')->default('')->comment('Tương thích');
            $table->string('charging_port')->default('')->comment('Cổng sạc');
            $table->string('used_time')->default('')->comment('Thời gian sử dụng');
            $table->string('full_charge')->default('')->comment('Thời gian sạc đầy');

            $table->string('connect_same')->default('')->comment('Kết nối cùng lúc');
            $table->string('connection_distance')->default('')->comment('Khoảng cách kết nối');
            $table->string('weight')->default('')->comment('Trọng lượng');

            $table->string('trademark')->default('')->comment('Thương hiệu');
            $table->string('manufacture')->default('')->comment('Nơi sản xuất (tùy từng lô hàng)');

            // Thời gian tồn tại
            $table->date('start_date')->nullable()->comment('Ngày bắt đầu');
            $table->date('end_date')->nullable()->comment('Ngày kết thúc');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accessories');
    }
}
