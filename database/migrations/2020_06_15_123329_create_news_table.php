<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();

            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');
            $table->string('title')->default('')->comment('Tiêu đề');
            $table->unsignedTinyInteger('content_type')->comment('Loại tin');
            $table->string('thumbnails')->comment('Ảnh đại diện');;
            $table->longText('content')->comment('Nội dung tin');
            $table->string('source')->comment('Ảnh đại diện');;

            // Thời gian tồn tại
            $table->date('start_date')->nullable()->comment('Ngày bắt đầu');
            $table->date('end_date')->nullable()->comment('Ngày kết thúc');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
