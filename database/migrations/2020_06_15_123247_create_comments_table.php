<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('product_id')->comment('Mã sản phẩm, khóa ngoại');

            // Liên hệ
            $table->string('contact_name')->default('')->comment('Họ tên người liên hệ');
            $table->string('contact_address')->default('')->comment('Địa chỉ liên hệ');
            $table->string('contact_mobile')->default('')->comment('SĐT liên hệ');
            $table->string('contact_email')->default('')->comment('Email liên hệ');
            $table->tinyInteger('sex')->default(1)->comment('Giới tính 0 = nữ, 1 = nam');

            $table->text('question')->comment('Lời nhắn');

            $table->index(['product_id']);

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
