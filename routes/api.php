<?php

use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Kiểm tra đăng nhập
Route::group(['prefix' => 'auth'], function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('list', 'AuthController@index');
});

// check logout
Route::group([
    'prefix' => 'auth',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('logout', 'AuthController@logout');
        $router->get('me', 'AuthController@me');
    });
});

// Tài khoản người sử dụng
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'user',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'UserController@index');
        $router->post('create', 'UserController@create');
        $router->get('show/{id}', 'UserController@show');
        $router->post('update/{id}', 'UserController@update');
        $router->delete('remove/{id}', 'UserController@remove');
        $router->post('searchAll', 'UserController@searchAll');
    });
});

// Product
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'product',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'ProductController@index');
        $router->post('create', 'ProductController@create');
        $router->get('show/{id}', 'ProductController@show');
        $router->post('update/{id}', 'ProductController@update');
        $router->delete('remove/{id}', 'ProductController@remove');
        $router->post('searchAll', 'ProductController@searchAll');
        $router->post('upload', 'ProductController@upload');
    });
});

// Clock
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'clock',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'ClockController@index');
        $router->post('create', 'ClockController@create');
        $router->get('show/{id}', 'ClockController@show');
        $router->post('update/{id}', 'ClockController@update');
        $router->delete('remove/{id}', 'ClockController@remove');
        $router->post('searchAll', 'ClockController@searchAll');
        $router->post('upload', 'ClockController@upload');
    });
});

// Accessory
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'accessory',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'AccessoryController@index');
        $router->post('create', 'AccessoryController@create');
        $router->get('show/{id}', 'AccessoryController@show');
        $router->post('update/{id}', 'AccessoryController@update');
        $router->delete('remove/{id}', 'AccessoryController@remove');
        $router->post('searchAll', 'AccessoryController@searchAll');
        $router->post('upload', 'AccessoryController@upload');
    });
});

// Order
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'order',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'OrderController@index');
        $router->get('show/{id}', 'OrderController@show');
        $router->delete('remove/{id}', 'OrderController@remove');
        $router->post('searchAll', 'OrderController@searchAll');
    });
});

// Comment
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'comment',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'CommentController@index');
        $router->delete('remove/{id}', 'CommentController@remove');
        $router->post('searchAll', 'CommentController@searchAll');
    });
});

// NEWS
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix' => 'news',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'NewsController@index');
        $router->post('create', 'NewsController@create');
        $router->get('show/{id}', 'NewsController@show');
        $router->post('update/{id}', 'NewsController@update');
        $router->delete('remove/{id}', 'NewsController@remove');
        $router->post('searchAll', 'NewsController@searchAll');
        $router->post('upload', 'NewsController@upload');
    });
});

// IMAGE UPLOAD
Route::post('image.upload', 'ImageController@upload');